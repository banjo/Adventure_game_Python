#!/usr/bin/python
# -*- coding: utf-8 -*-
import funcs
import base
from sys import stdout as Out
#shop table blocks (shopping)
#____________________________S  H  O  P __________________________________


def shcase(Char, Item, Data):  # string n,string t?????
    if Item.type.name == "Armor" :
        if Char.insertArmor(Char, Item, Data.Itms):
            print("You bought " + Item.name + " armor!!! \n")
        return

    if(Char.gold >= Item.Bgold):
        if (Char.insertItem(Item)):
            print("You bought {} {} !!! \n".format(Item.name, Item.type.name))
            funcs.prop(Char, Item)
        Char.gold = Char.gold - Item.Bgold

    else:
        print("You dont have enough gold!\n")


#check shop files
def shop_check(itms, shop_v, loc):

#SteamReader = loc.c_str())
    stream = open(loc, "r")
    lines = stream.readlines()

    for line in lines:
        lineSplit = line.split(",")
        for it in itms:
            if (it.name == lineSplit[0] and it.type.name == lineSplit[1]):
                shop_v.append(it)
                break

    if(len(shop_v) != 8):
        return False

    return True


#Create shop table
def shop_table(v):

    funcs.clear()
    print(" ╔═══════════════════════════════════════════════════════════════════════════════╗ ")
    print(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ")
    print(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ")
    print(" ║ ░░░░░╔═════1══════╗░░░░░╔═════2══════╗░░░░░╔═════3══════╗░░░░░╔═════4══════╗░░░░░ ")
    print(" ║ ░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░" % (v[0].name.center(12), v[1].name.center(12), v[2].name.center(12), v[3].name.center(12)))
    print(" ║ ░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░" % (v[0].type.name.center(12), v[1].type.name.center(12), v[2].type.name.center(12), v[3].type.name.center(12)))
    print(" ║ ░░░░░║ Attack: %s║░░░░░║ Attack: %s║░░░░░║ Attack: %s║░░░░░║ Attack: %s║░░░░░" % (str(v[0].attack).ljust(3), str(v[1].attack).ljust(3), str(v[2].attack).ljust(3), str(v[3].attack).ljust(3)))
    print(" ║ ░░░░░║ Defenc: %s║░░░░░║ Defenc: %s║░░░░░║ Defenc: %s║░░░░░║ Defenc: %s║░░░░░" % (str(v[0].defence).ljust(3), str(v[1].defence).ljust(3), str(v[2].defence).ljust(3), str(v[3].defence).ljust(3)))
    print(" ║ ░░░░░║ Agilit: %s║░░░░░║ Agilit: %s║░░░░░║ Agilit: %s║░░░░░║ Agilit: %s║░░░░░" % (str(v[0].agility).ljust(3), str(v[1].agility).ljust(3), str(v[2].agility).ljust(3), str(v[3].agility).ljust(3)))
    print(" ║ ░░░░░║   Cost:%s║░░░░░║   Cost:%s║░░░░░║   Cost:%s║░░░░░║   Cost:%s║░░░░░" % (str(v[0].Bgold).center(4), str(v[1].Bgold).center(4), str(v[2].Bgold).center(4), str(v[4].Bgold).center(4)))
    print(" ║ ░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░ ")
    print(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ")
    print(" ║ ░░░░░░░░░░░░░░░░░░░░░░❯❯❯❯❯❯❯❯ CHOOSE WHICH U WANT❮❮❮❮❮❮❮❮░░░░░░░░░░░░░░░░░░░░░░░ ")
    print(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ")
    print(" ║ ░░░░░╔═════5══════╗░░░░░╔═════6══════╗░░░░░╔═════7══════╗░░░░░╔═════8══════╗░░░░░ ")
    print(" ║ ░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░" % (v[4].name.center(12), v[5].name.center(12), v[6].name.center(12), v[7].name.center(12)))
    print(" ║ ░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░║%s║░░░░░" % (v[4].type.name.center(12), v[5].type.name.center(12), v[6].type.name.center(12), v[7].type.name.center(12)))
    print(" ║ ░░░░░║ Attack: %s║░░░░░║ Attack: %s║░░░░░║ Attack: %s║░░░░░║ Attack: %s║░░░░░" % (str(v[4].attack).ljust(3), str(v[5].attack).ljust(3), str(v[6].attack).ljust(3), str(v[7].attack).ljust(3)))
    print(" ║ ░░░░░║ Defenc: %s║░░░░░║ Defenc: %s║░░░░░║ Defenc: %s║░░░░░║ Defenc: %s║░░░░░" % (str(v[4].defence).ljust(3), str(v[5].defence).ljust(3), str(v[6].defence).ljust(3), str(v[7].defence).ljust(3)))
    print(" ║ ░░░░░║ Agilit: %s║░░░░░║ Agilit: %s║░░░░░║ Agilit: %s║░░░░░║ Agilit: %s║░░░░░" % (str(v[4].agility).ljust(3), str(v[5].agility).ljust(3), str(v[6].agility).ljust(3), str(v[7].agility).ljust(3)))
    print(" ║ ░░░░░║   Cost:%s║░░░░░║   Cost:%s║░░░░░║   Cost:%s║░░░░░║   Cost:%s║░░░░░" % (str(v[4].Bgold).center(4), str(v[5].Bgold).center(4), str(v[6].Bgold).center(4), str(v[7].Bgold).center(4)))
    print(" ║ ░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░ ")
    print(" ╚═░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ")
    print("   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ PRESS 9 TO EXIT SHOP ░░░░░░░░░░░░ \n")


# S H O P
def shop(Char, Data):
    v = []
    loc = Data.Loc[base.cur_Loc].shop_tbl
    if not shop_check(Data.Itms, v, loc):
        print("Shieeet Brawh! Something Wrong n")
        input()
        return

    shop_table(v)

    while(True):
            p = 0
            Out.write("Enter Number: ")  # buying weapon or armor
            p = funcs.Input()
            if (p > 0 and p < 9):
                shcase(Char, v[p - 1], Data)
            else:
                break
    funcs.day_count(Char, 0, 0, 15)
    return
