#!/usr/bin/python
# -*- coding: utf-8 -*-
import funcs
import base
from enum import Enum
from sys import stdout as Out
from quests import Quests_info
import quests


class GearType(Enum):
    Weapon1 = 0
    Weapon2 = 1
    Head = 2
    Chest = 3
    Pants = 4
    Boots = 5


class InvType(Enum):
    NoItem = 0
    Sword = 1
    Axe = 2
    Bow = 3
    Shield = 4
    Head = 5
    Chest = 6
    Pants = 7
    Boots = 8
    Armor = 9
    Modifier = 10
    QuestItem = 11
    Potion = 12
    Other = 13


class Item:

    def __init__(self, name="empty", tp="NoItem",
                 attack=0, defence=0, agility=0, Bgold=0, Sgold=0):
        self.name = name
        self.type = InvType[tp]
        self.attack = attack
        self.defence = defence
        self.agility = agility
        self.Bgold = Bgold
        self.Sgold = Sgold

    def print_it(self):
        print("{} {} {} {} {} {} {} "
              .format(self.name, str(self.type),
                      self.attack, self.defence, self.agility,
                      self.Bgold, self.Sgold))


class Inventory:

    curIndex = 0
    Empty = Item()

    # Constr
    def __init__(self):
        self.Gear = [Item(), Item(), Item(), Item(), Item(), Item()]
        self.Gear[2].type = InvType.Head
        self.Gear[3].type = InvType.Chest
        self.Gear[4].type = InvType.Pants
        self.Gear[5].type = InvType.Boots
        self.Inv = [Item()] * 50

    def isEmpty(self):

        for i in range(base.invSize):  # invsize
            if self.Inv[i].type == InvType.NoItem:
                self.curIndex = i
                return True
        return False

    def insertItem(self, i):

        if (self.isEmpty()):
            self.Inv[self.curIndex] = i
            return True

        else:
            print("MOVE BITCH!")

        return False

    def insertArmor(self, xx, i, inv):

        if(xx.gold >= i.Bgold and self.isEmpty()):
            for k in range(18, 38):
                if(inv[k].name == i.name and inv[k].type != InvType.Armor):
                    for ind in range(base.invSize):
                        if (self.Inv[ind].type == InvType.NoItem):
                            self.Inv[ind] = inv[k]
                            break

            xx.gold = xx.gold - i.Bgold
            return True

        else:
            print("You dont have enough gold or Your Inventory is Full\n")
            return False

    def delete(self, index):
        self.Inv[index] = self.Empty  # swap empty and current item

    # Move Item to Inventory
    def MoveToInv(self, X, G):
        if (self.isEmpty()):
            funcs.del_prop(X, self.Gear[G])
            self.Inv[self.curIndex] = self.Gear[G]
            self.Gear[G] = self.Empty

        else:
            print("U cant bitch!")
            input()
        funcs.day_count(self, 0, 0, 10)

        # Move Item to Gear
    def MoveToGear(self, X, index):
        weapons = ["Sword", "Bow", "Axe", "Shield"]

        if self.Inv[index].type.name in weapons:
            if self.Gear[0].type == InvType.NoItem:
                funcs.prop(X, self.Inv[index])

                self.Gear[0] = self.Inv[index]
                self.Inv[index] = self.Empty

            elif self.Gear[1].type == InvType.NoItem:
                funcs.prop(X, self.Inv[index])

                self.Gear[1] = self.Inv[index]
                self.Inv[index] = self.Empty
            else:
                print("Your Weapon Slots is Full! Do you want to change itmes? [Y/N ]: ")
                if input() == "Y":
                    print("Choose Slot: ")
                    slot = int(input())

                    funcs.del_prop(X, self.Gear[slot - 1])
                    funcs.prop(X, self.Inv[index])
                    swap = self.Gear[slot - 1]
                    self.Gear[slot - 1] = self.Inv[index]
                    self.Inv[index] = swap

                    print("Changed!")
                    input()

        else:
            for x in range(len(self.Gear)):
                if self.Gear[x].type == self.Inv[index].type:
                    funcs.prop(X, self.Inv[index])
                    swap = self.Gear[x]
                    self.Gear[x] = self.Inv[index]
                    self.Inv[index] = swap
                    return
        funcs.day_count(self, 0, 0, 10)


class Locations:

    #  Constructors
    def __init__(self, name, shop, price, firstTime):
        self.loc_name = name
        self.shop_tbl = shop
        self.price = price
        self.firstTime = firstTime
        self.locked = True
        self.func = []

    #  travel to town

    def town(self, Data, Char):
        if Data.Loc[base.cur_Loc].loc_name == self.loc_name:
            print("You are here u dumbass!!! :D ")
            input()
        elif Char.gold < self.price:
            print("You don't have enough money to visit this location!!!")
            print("Press enter to continue...")
            input()
        else:
            n_town = Data.Loc.index(self)

            if base.cur_Loc > n_town:
                funcs.day_count(Char, (base.cur_Loc - n_town))
            else:
                funcs.day_count(Char, (n_town - base.cur_Loc))

            Char.gold = Char.gold - self.price
            base.cur_Loc = Data.Loc.index(self)
        return


     #  Choose Town
def travel(Char, Data):
    print("Choose Destination")
    loc = []
    i = 0
    for x in range(len(Data.Loc)):
        if not Data.Loc[x].locked:
            print("%d. %s - cost: %d" % ((i + 1), Data.Loc[x].loc_name.ljust(20), Data.Loc[x].price))
            loc.append(Data.Loc[x])
            i += 1
    print("%d. EXIT " % (i + 1))

    trloc = funcs.Input(">> ")
    if 0 > trloc or trloc > i:
        return

    loc[trloc - 1].town(Data, Char)

    return


# Create Inventory table
def inv_table(c):
    """Shows Gear and Inventory Tables"""
    while (True):
        names = []
        attack = []
        defence = []
        agility = []
        types = []

        for n in c.Gear:
            names.append(n.name.center(12))
            types.append(n.type.name.center(12))
            attack.append(str(n.attack).ljust(3))
            defence.append(str(n.defence).ljust(3))
            agility.append(str(n.agility).ljust(3))
        w1 = c.Gear[0].type.name.center(12) if c.Gear[0].type != InvType.NoItem else "Weapon1".center(12)
        w2 = c.Gear[1].type.name.center(12) if c.Gear[1].type != InvType.NoItem else "Weapon2".center(12)

        funcs.clear()
        print(" ╔═══════════════════════════════════════════════════════════════════════════╗")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ ")
        print("▒▒▒▒ Gold:" + str(c.gold).ljust(8) + "▒▒▒▒▒▒▒▒▒▒▒   W E A P O N S   ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╔═════1══════╗▒▒▒▒▒▒▒▒▒▒▒▒╔═════2══════╗▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║%s║▒▒▒▒▒▒▒▒▒▒▒▒║%s║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ " % (names[0], names[1]))
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║%s║▒▒▒▒▒▒▒▒▒▒▒▒║%s║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ " % (w1, w2))
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║ Attack: %s║▒▒▒▒▒▒▒▒▒▒▒▒║ Attack: %s║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ " % (attack[0], attack[1]))
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║ Defenc: %s║▒▒▒▒▒▒▒▒▒▒▒▒║ Defenc: %s║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ " % (defence[0], defence[0]))
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║ Agilit: %s║▒▒▒▒▒▒▒▒▒▒▒▒║ Agilit: %s║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ " % (agility[0], agility[1]))
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╚════════════╝▒▒▒▒▒▒▒▒▒▒▒▒╚════════════╝▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ___   ___   ___    ___   ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ |     |     |   |  |   |  ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     A R M O R     ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ |  _  |--   |---|  |---   ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ |___| |___  |   |  |   \\ ")
        print("▒▒▒▒╔═════3══════╗▒▒▒▒╔═════4══════╗▒▒▒▒╔═════5══════╗▒▒▒▒╔═════6══════╗▒▒▒▒ ║ ")
        print("▒▒▒▒║%s║▒▒▒▒║%s║▒▒▒▒║%s║▒▒▒▒║%s║▒▒▒▒ ║ " % (names[2], names[3], names[4], names[5]))
        print("▒▒▒▒║%s║▒▒▒▒║%s║▒▒▒▒║%s║▒▒▒▒║%s║▒▒▒▒ ║ " % (types[2], types[3], types[4], types[5]))
        print("▒▒▒▒║ Attack: %s║▒▒▒▒║ Attack: %s║▒▒▒▒║ Attack: %s║▒▒▒▒║ Attack: %s║▒▒▒▒ ║ " % (attack[2], attack[3], attack[4], attack[5]))
        print("▒▒▒▒║ Defenc: %s║▒▒▒▒║ Defenc: %s║▒▒▒▒║ Defenc: %s║▒▒▒▒║ Defenc: %s║▒▒▒▒ ║ " % (defence[2], defence[3], defence[4], defence[5]))
        print("▒▒▒▒║ Agilit: %s║▒▒▒▒║ Agilit: %s║▒▒▒▒║ Agilit: %s║▒▒▒▒║ Agilit: %s║▒▒▒▒ ║ " % (agility[2], agility[3], agility[4], agility[5]))
        print("▒▒▒▒╚════════════╝▒▒▒▒╚════════════╝▒▒▒▒╚════════════╝▒▒▒▒╚════════════╝▒▒▒▒ ║ ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║ ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ╝  ")
        print("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒   \n")
        col = 0
        print("╔══════════════════════════════════════════════════════════════════════════╗")

        for i in range(base.invSize):
            if c.Inv[i].name == "empty":
                Out.write("║               empty                ║")
            else:
                Out.write("║ {} {} At:{} Df:{} Ag:{} ║"
                          .format(c.Inv[i].name.ljust(6), c.Inv[i].type.name.ljust(6),
                                  str(c.Inv[i].attack).ljust(3),
                                  str(c.Inv[i].defence).ljust(3),
                                  str(c.Inv[i].agility).ljust(3)))
            col += 1
            if col % 2 == 0:
                print("({}-{})".format(i, i + 1))
            elif i == base.invSize - 1 and base.invSize % 2 != 0:
                print("║XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX║({}-{})".format(i, i + 1))

        print("╚══════════════════════════════════════════════════════════════════════════╝")

        print("JUST DO IT!!!!")
        print("1. Move to Inventory ")
        print("2. Move to Gear ")
        print("3. Sell Item ")
        print("4. Gold -> XP")
        print("5. Exit ")

        inv_f = funcs.Input(">> ")

        if inv_f == 1:
            Out.write("\nChoose the Slot: ")
            MTI = funcs.Input() - 1
            c.MoveToInv(c, MTI)

        elif inv_f == 2:
            Out.write("\nChoose the Index of Item: ")
            MTG = funcs.Input() - 1
            c.MoveToGear(c, MTG)

        elif inv_f == 3:
            Out.write("\nChoose the Index of Item: ")
            itm = funcs.Input() - 1
            c.gold = c.gold + c.Inv[itm].Sgold
            print("You earned %d Gold." % c.Inv[itm].Sgold)
            c.delete(itm)
            funcs.day_count(c, 0, 0, 10)
            input()

        elif inv_f == 4:
            gold = funcs.Input("Input Gold: ")
            if gold <= c.gold:
                c.GoldToXp(gold)
            else:
                input("You don't have enough money!")

        else:
            return


# Create Map
def Map(dat):
    """Prints Map"""
    l = ["   "] * 11

    l[base.cur_Loc] = "You"
    X = [None] * len(dat)

    for i in range(len(dat)):
        if(dat[i].locked):  # is true
            X[i] = 'X'
        else:
            X[i] = '.'

    Leg = []  # Legend

    for ind in range(len(dat)):
        if(X[ind] == '.'):
            Leg.append(dat[ind].loc_name.ljust(18).replace("_", " "))
        else:
            Leg.append(" " * 18)

    """********************
    0. Nemertea  =  home town
    1. Flaming Stones
    2. Wizard Tower
    3. Shadow Mountain
    4. Golden Forest
    5. Dead Lake
    6. Orc's Stronghold
    7. Hjortar Hus
    8. Black Fall
    9. Final BossAssNigga
    *********************"""
    funcs.clear()

    print("╔════════════════════════════════════════════════════════════════════════════════════════════════════╗\n" +
          "║                          ~~~~~~           .. -N-                                           .       ║\n" +
          "║......                   | " + l[8] + "  |       .-     |                .                  `......-         ║\n" +
          "║     :`                  | B F  |       :                  .....                  .-                ║\n" +
          "║     ..   ~~~~~~          ~~~~~~       -.                 -.                    ~~~~~~              ║\n" +
          "║     `:--| " + l[6] + "  | " + X[6] + ".-.        :       :`                ~~~~~~                 | " + l[4] + "  |             ║\n" +
          "║      :` | O S  |    `-..```.." + X[8] + "....../         ....." + X[1] + ".-| " + l[1] + "  |......   .      | G F  |             ║\n" +
          "║     `:   ~~~~~~         ```         :.    -...       `| F S  |              ./ ~~~~~~              ║\n" +
          "║    `:                                .- --             ~~~~~~             --" + X[4] + "                      ║\n" +
          "║    -`          ..                     :                  :               /                         ║\n" +
          "║    :`        :.  -.                   :                  :`             :                          ║\n" +
          "║    /         :    :                ~~~~~~~            `-..              :                 `        ║\n" +
          "║    `" + X[7] + "`        :   `.-`            |  " + l[0] + "  |           :`       `.      `-              `.-`        ║\n" +
          "║    ~~~~~~~~   :      :`       ....|  NE   |..-.        `.../-..`       /..-`         .-.           ║\n" +
          "║   |  " + l[7] + "   | -.      `:      :`    ~~~~~~~    -.            ...........`   `:.    .--`             ║\n" +
          "║   |        |" + X[7] + "-        :-`  `:`        :        `+......                      `-.:-`                ║\n" +
          "║ | |  H  H  |           .:--.          :        :`      --.        --:            .....`          | ║\n" +
          "║ W- ~~~~~~~~           -.               :       :          .......-  -.                ......... -E ║\n" +
          "║ |                  .--             `-..`       `....`                :.-.      `..               | ║\n" +
          "║              /-----                :`               .-.              :  `......`  ..               ║\n" +
          "║             .`               .-....`--                :              .-                          ..║\n" +
          "║           /`                 /        ........        `               /                         `- ║\n" +
          "║           |              `...`                -.                      `:              .......`..-  ║\n" +
          "║           /             `:                     .`                      -..```   ...-.        `     ║\n" +
          "║  ~~~~~~ -" + X[5] + ".-.         ..." + X[3] + "                     `|" + X[9] + "----.                    ``.:/:                  ║\n" +
          "║ | " + l[5] + "  |`   ':........    |               ......'    ~~~~~~~~~~                 : ..``             ║\n" +
          "║ | D L  |       .:         ~~~~~         -.          |   " + l[9] + "    |                :  ~~~~~~~         ║\n" +
          "║  ~~~~~~          :       | " + l[3] + " |" + X[3] + "......:            |  FINAL   |                : |  " + l[2] + "  |        ║\n" +
          "║                  :       | S M |                    |   BOSS   |                " + X[2] + ".|  W T  |        ║\n" +
          "║                  :        ~~~~~               |     |          |                   ~~~~~~~         ║\n" +
          "║                  :                           -S-     ~~~~~~~~~~                                    ║\n" +
          "╠═════════════════════════╦═══════════════════< Legend >═══════════════════════╦═════════════════════╣\n" +
          "║ NE +> " + Leg[0] + "║ OS +> " + Leg[6] + "║ GF  +> " + Leg[4] + "║ WT +> " + Leg[2][:14] + "║\n" +
          "║ HH +> " + Leg[7] + "║ SM +> " + Leg[3] + "║ FS  +> " + Leg[1] + "║ DL +> " + Leg[5][:14] + "║\n" +
          "║ BF +> " + Leg[8] + "║ X  +> BARIER            ║ YOU +> Current location  ║                     ║\n" +
          "╚═════════════════════════╩═════════════════════════╩══════════════════════════╩═════════════════════╝")


# INVENTORY
def profile(Char, data):
    """Prints Character's profile Menu"""
    while (True):

        funcs.clear()
        Char.update_info()  # update character's information
        Char.print_info()
        print("")

        print("1. Inventory ")
        print("2. Map ")
        print("3. Quests")
        print("4. Exit ")

        in_ = funcs.Input(">> ")

        if in_ == 1:
            inv_table(Char)
        elif in_ == 2:
            Map(data.Loc)
            print("\nType \"Quit\" to go back!")
            invController = ""
            while invController != "quit":
                #  get tolower and check
                invController = input(">> ")
                invController = invController.lower()
            funcs.day_count(Char, 0, 0, 10)
        elif in_ == 3:
            quests.update(data, Char)
            Quests_info(Char)
            funcs.day_count(Char, 0, 0, 15)

        elif in_ >= 4:
            break

 # while inv
