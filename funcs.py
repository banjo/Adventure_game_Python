#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import base
import time
import datetime
from sys import stdout as Out
from random import randint, choice
from characters import Character
from items import Item


def Intro():
    clear()
    with open("Data/Intro.dat") as N:
        for x in range(12):
            print(N.readline().rstrip("\n"))
            time.sleep(0.02)
        time.sleep(0.7)
        print("\t\t\t\t\t\t\tBIATCH")
        time.sleep(1)
        input("\n\n\t\t\t < Press Enter To Start... >")
        N.close()


# Console Clear
def clear():
    if(os.name == 'nt'):  # if Windows
        os.system("cls")
    else:  # Other OS
        os.system("clear")


def Input(in_string=''):
    """ Input for numbers """
    X = ''
    while True:
        try:
            X = input(in_string)
            return int(X)
        except ValueError:
            if X == "quit" or X == "back":
                return X
            else:
                print("Number Bitch!")


def insert_string(string, box_size):
    words = string.split(" ")
    box = ['']
    cc = box_size
    h = 0
    for st in words:
        if len(st) < box_size:  # if there is space
            box_size -= len(st) + 1
            box[h] += "%s " % st
            if st == words[-1]:  # if last string close line
                box[h] += " " * (box_size)
                h += 1
        else:  # if is not, move to new line
            box[h] += " " * box_size
            h += 1
            box_size = cc
            box.append('')  # start new line
            if st == words[-1]:
                box[h] += "{}{}".format(st, " " * (cc - len(st)))
                h += 1
            else:
                box_size -= len(st) + 1
                box[h] += "%s " % st

    return box


def print_story(string, teller='', speed=1):

    words = string.split(' ')
    print("\n  %s:" % teller)
    for word in words:
        for char in word:
            Out.write(char)
            Out.flush()
            time.sleep(0.05 / speed)
            if char == '.':
                time.sleep(0.3 / speed)
            elif char in [',', '!', '?']:
                time.sleep(0.1 / speed)
        Out.write(" ")
    print("")
    time.sleep(0.7 / speed)


def print_cloud(string, name='', x=44, y=44):
    """ print cloudy messeges like chat
        print_cloud("a sting", "teller", minsize=44, maxsize=44)
    like dis V
        ┌─────────────────────────────────────────────┐
        │                                             ││- - - - - - -
        └─────────────────────────────────────────────┘│- - - - - - -
          ─────────────────────────────────────────────┘- - - - - - -
    """

    cloud_size = randint(x, y)
    Bubble = insert_string(string, cloud_size - 2)

    # Now print :D
    if name == "Me":
        print("\t\t\t\t %s:  " % name.rjust(cloud_size))
        print("\t\t\t\t ┌{}┐".format("─" * cloud_size))  # print first 2 Lines
        for x in Bubble:
            print(" - - - - - - - - - - - - - - - -││ %s │" % x)
        print(" - - - - - - - - - - - - - - - -│└{}┘".format("─" * cloud_size))
        print(" - - - - - - - - - - - - - - - -└{}\n\n\n".format("─" * cloud_size))
        input()
    else:
        print("  %s:" % name)
        print("  ┌{}┐\t\t\t\t".format("─" * cloud_size))  # print first 2 Lines
        for x in Bubble:
            print("  │ %s ││- - - - - - - - - - - - - - - -" % x)
        print("  └{}┘│- - - - - - - - - - - - - - - -".format("─" * cloud_size))
        print("   {}┘- - - - - - - - - - - - - - - -\n\n\n".format("─" * cloud_size))
        input()


def print_alert(string):
    alrt_size = 50
    Alert = insert_string(string, alrt_size - 2)
    print("\t\t⚠{}⚠".format("⚠" * alrt_size))
    if len(Alert) == 1:
        print("\t\t⚠%s⚠" % string.center(alrt_size, "⚠"))
    else:
        for x in Alert:
            print("\t\t⚠ %s ⚠" % x)
    print("\t\t⚠{}⚠".format("⚠" * alrt_size))
    input()


def day_count(Char, day=0, hour=0, minute=0):
    Char.time += datetime.timedelta(days=day, seconds=(hour * 3600 + minute * 60))


# Set Items's properties
def prop(Char, Item):
    Char.attack = Char.attack + Item.attack
    Char.defence = Char.defence + Item.defence
    Char.stamina = Char.stamina + Item.agility


# delete Item's properties
def del_prop(Char, Item):  # delete Items's properties
    Char.attack = Char.attack - Item.attack
    Char.defence = Char.defence - Item.defence
    Char.stamina = Char.stamina - Item.agility


# Set human's default Gear
def hum_def_items(Char, itms):
    Char.Gear[0] = itms[1]
    Char.Gear[1] = itms[4]
    Char.Gear[3] = itms[19]
    prop(Char, itms[1])
    prop(Char, itms[4])
    prop(Char, itms[19])


# Set orc's default Gear
def orc_def_items(Char, itms):
    Char.Gear[0] = itms[7]
    Char.Gear[1] = itms[9]
    Char.Gear[2] = itms[22]
    prop(Char, itms[7])
    prop(Char, itms[9])
    prop(Char, itms[22])


# Set elf's default Gear
def elf_def_items(Char, itms):
    Char.Gear[0] = itms[2]
    Char.Gear[1] = itms[6]
    prop(Char, itms[2])
    prop(Char, itms[6])


# Drop Item
def dropper(Char, items):
    Awards = {
        0: [x for x in items if x.name == "Wooden"],
        5: [x for x in items if x.name == "Iron"],
        10: [x for x in items if x.name == "Golden"],
        15: [x for x in items if x.name == "Dragon"]
    }
    for x in Awards.values():
        x.append(Char.Empty)

    for l in reversed(list(Awards.keys())):
        if Char.lvl >= l:
            it = choice(Awards[l])
            if it.name != "empty":
                Char.insertItem(it)
                prop(Char, it)
                print("Dropped %s %s " % (it.name, it.type.name))
                input()
            return


# printcharacter's information for battle or sam sungs
def printChars(chars):

    for x in chars:
        x.update_info()

    for i in range(len(chars[0].info)):
        for j in range(len(chars)):
            if i == 0 or i == len(chars[0].info):
                Out.write(chars[j].info[i] + "+")
            else:
                Out.write(chars[j].info[i] + "|")

        print('')

    return


def messeges(Char, data):
    """Prints Menu Messeges"""
    if data.Loc[base.cur_Loc].firstTime:
        #  Do everything for the first time here
        clear()
        print("Welcome, {} to {}!".format(Char.name, data.Loc[0].loc_name))
        data.Loc[base.cur_Loc].firstTime = False
    #Get the welcome messages ready for the first time and close the file
    else:
        #  Not first time already just show message from a static List
        if data.Msgs.count != 0:
            clear()
            print(choice(data.Msgs))  # display
            #print("BIATCH")
        else:
            print("Welcome back, " + Char.name + "!")


def Save(you, dat):
    if not os.path.exists("Saves"):
        os.makedirs("Saves")
    filec = 1
    files = []
    path = ""
    sav = input("Save file? [Y/n]: ")
    if sav == "n":
        return
    clear()
    print("Save Files: ")
    print("%d ) %s" % (filec, "Create New File"))
    for line in os.listdir("Saves"):
        filec += 1
        x = line.rstrip(".plr")
        files.append(x)
        print("%d ) %s" % (filec, x))

    p_ = Input("\n>> ")
    if p_ == 1:
        path = input("Input Name: ") + ".plr"
    else:
        path = files[p_ - 2] + ".plr"

    stream = open("Saves/" + path, 'w')

    # Char properties
    stream.write("%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d" % (you.name, you.strength, you.health, you.speed,
                                                             you.attack, you.defence, you.stamina,
                                                             you.xp, you.max_xp, you.lvl, you.gold, you.isplayer, you.kills))

    stream.write("\n")  # newline

    # Location's firstime
    for f in dat.Loc:
        if f == dat.Loc[-1]:
            stream.write(str(int(f.firstTime)))
        else:
            stream.write(str(int(f.firstTime)) + ",")

    stream.write("\n")  # newline

    # Locked or nah
    for l in dat.Loc:
        if l == dat.Loc[-1]:
            stream.write(str(int(l.locked)))
        else:
            stream.write(str(int(l.locked)) + ",")

    # current location
    stream.write("\n{}".format(base.cur_Loc))
    # date and time
    stream.write("\n{}".format(you.time))
    # Char's Gear
    for x in you.Gear:
        stream.write("\n%s,%s,%d,%d,%d,%d,%d" % (x.name, x.type.name, x.attack, x.defence, x.agility, x.Bgold, x.Sgold))

    # Char's Inventory
    for x in you.Inv:
        stream.write("\n%s,%s,%d,%d,%d,%d,%d" % (x.name, x.type.name, x.attack, x.defence, x.agility, x.Bgold, x.Sgold))

    # Char's Quests
    for x in you.Quests:
        stream.write("\n")
        stream.write("\n" + x.name)
        stream.write("\n" + x.info)
        stream.write("\n%d" % len(x.triger))
        for tr in x.triger:
            tsk = "\n{},{}".format(tr[0], int(tr[1]))
            stream.write(tsk)
        stream.write("\n" + x.status.name)
        stream.write("\n" + str(x.difficulty))

    stream.close()


def Load(dat):
    filec = 1
    files = []
    path = ""
    clear()
    print("Load Files: ")
    for line in os.listdir("Saves"):
        x = line.rstrip(".plr")
        files.append(x)
        print("%d ) %s" % (filec, x))
        filec += 1

    p_ = Input("\n>> ")
    path = files[p_ - 1] + ".plr"

    stream = open("Saves/" + path, 'r')

    char = stream.readline().rstrip("\n")
    you = char.split(',')
    # Input Char
    c = Character(you[0], int(you[1]), int(you[2]), int(you[3]), int(you[4]), int(you[5]), int(you[6]),
                  int(you[7]), int(you[8]), int(you[9]), int(you[10]), bool(int(you[11])), int(you[12]))

    # Reads Location's firstime visit
    loc = stream.readline().rstrip('\n')
    loc_time = loc.split(',')
    for f in range(len(dat.Loc)):
        dat.Loc[f].firstTime = bool(int(loc_time[f]))

    # Reads Location's firstime visit
    loc = stream.readline().rstrip('\n')
    lock = loc.split(',')
    for l in range(len(dat.Loc)):
        dat.Loc[l].locked = bool(int(lock[l]))

    # Current Location
    curL = stream.readline().rstrip("\n")
    base.cur_Loc = int(curL)

    # Import time
    n = time.strptime(stream.readline().rstrip("\n"), "%Y-%m-%d %H:%M:%S")
    c.time = datetime.datetime(n.tm_year, n.tm_mon, n.tm_mday, n.tm_hour, n.tm_min, n.tm_sec)

    # Reads Char's Gear
    for g in range(len(c.Gear)):
        it = stream.readline().rstrip("\n").split(',')
        X = Item(it[0], it[1], int(it[2]), int(it[3]), int(it[4]), int(it[5]), int(it[6]))
        c.Gear[g] = X

    # Reads Char's Inventory
    for inv in range(len(c.Inv)):
        it = stream.readline().rstrip("\n").split(',')
        X = Item(it[0], it[1], int(it[2]), int(it[3]), int(it[4]), int(it[5]), int(it[6]))
        c.Inv[inv] = X
    base.inputQuests(c, stream)
    dat.Chars.append(c)  # add to base
    stream.close()
