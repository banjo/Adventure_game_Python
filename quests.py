#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
@ piroba (1 ,2 ,3, 4,....)
@ statusi [Active, Passive, Done] [Main, Side]
@ questis shesrulebis procentuli machvenebeli
"""

from enum import Enum
import base
import funcs
from random import randint
from random import choice
import items
from sys import stdout as In


class Quest_Status(Enum):
    Idle = 0
    Active = 1
    Passive = 2
    Done = 3
    Completed = 4


class Quest(object):
    """Gandalf is Cuming"""
    def __init__(self, name, info, triger, status, diff):
        """ Quest Constructor. new_quest = Quest( Quest's Task Count )"""
            # Info
        self.name = name
        self.info = info
            # Trigers
        self.triger = triger
            # Status
        self.status = Quest_Status[status]
        self.difficulty = int(diff)
            # Award
        self.drop = [base.Base.Itms[2], items.Inventory.Empty]
        self.logs = []

    def perc(self):
        """[░░░░░░░░░░░░░░░░░░░░]100 %"""
        percent = 0
        for x in self.triger:
            percent += round(x.count(True) * 100 / len(self.triger))
        bar = "░" * round(percent / 5)
        return "[{}]{} %".format(bar.ljust(20), percent)

    def complete(self, Char):
        xp = randint(10, 20) * self.difficulty
        itm = choice(self.drop)
        if(self.status == Quest_Status.Done):
            Char.insertItem(itm)
            Char.addxp(xp)
            self.status = Quest_Status.Completed

    def check_gold(self, Char):
        gold = ""
        for tr in self.triger:
            if "gold" in tr[0]:
                gold = tr[0].strip("Collect gold")
                if Char.gold >= int(gold):
                    tr[1] = True
                    self.logs.append("Collected %s Gold" % gold)

    def check_xp_lvl(self, Char, x_l="xp"):  # and LVL
        for tr in self.triger:
            if x_l in tr[0]:
                xorl = tr[0].strip("Reach %s" % x_l)
                if getattr(Char, x_l) >= int(xorl):
                    tr[1] = True
                    self.logs.append("Reached %s %s" % (xorl, x_l))
                    #funcs.print_alert("Reached %s %s" % (xorl, x_l))

    def check_Item(self, Char):
        allitems = []
        allitems.extend(Char.Inv)
        allitems.extend(Char.Gear)
        for tr in self.triger:
            if "Find" in tr[0]:
                it = tr[0].strip("Find ").split(" ")
                for x in allitems:
                    if x.name == it[0] and x.type == items.InvType[it[1]]:
                        tr[1] = True
                        self.logs.append("Found %s %s" % it[0], it[1])

    def check_kills(self, Char):
        for tr in self.triger:
            if "Kill" in tr[0]:
                kll = tr[0].strip("Kill men")
                if Char.kills >= int(kll):
                    tr[1] = True
                    self.logs.append("Killed %s men" % kll)

    def check_town(self, Loc):
        for tr in self.triger:
            if "Visit" in tr[0]:
                lc = tr[0].strip("Visit ")
                for x in Loc:
                    if x.loc_name == lc and not x.firstTime:
                        tr[1] = True
                        self.logs.append("Visited %s " % x.loc_name)

    def check_time(self, Char):
        # dasamatebelia sikvdilianoba...
        for tr in self.triger:
            if "Survive" in tr[0]:
                d = tr[0].strip("Survive days")
                if Char.time.day >= int(d):
                    tr[1] = True
                    self.logs.append("Survived %s days" % d)


def print_status(Char, head, St):
    qfunc = {
        Quest_Status.Active: "Deactivate",
        Quest_Status.Passive: "Activate",
        Quest_Status.Done: "Collect",
        Quest_Status.Completed: "View"
    }

    ln = 53
    _c = 0
    status = [x for x in Char.Quests if x.status == St]
    if len(status) == 0:
        print("║ %s ║" % "EMPTY".center(ln))
    else:
        for i in range(len(status)):
            print("║ @%d %s %s║" % (i + 1, (status[i].name + ":").ljust(22), status[i].perc().ljust(28)))
            print("║    %s ║" % status[i].info.ljust(ln - 3))
    print("╚═══════════════════════════════════════ %d or Back ═════╝" % (len(status) + 1))
    a_ = funcs.Input(">> ")
    if a_ > len(status):
        return
    else:
        a_ -= 1
    funcs.clear()
    In.write(head)
    if a_ == "back" or a_ < len(status):
        ind = 0
        for n in status[a_].triger:
            ind += 1
            print("║ Task %d %s %s ║" % (ind, n[0].ljust(40), str(n[1]).ljust(5)))
        print("╚═══════════════════════════════════════════════════════╝")
        print("1. %s" % qfunc[St])
        print("2. Exit")
        _c = funcs.Input("\n>> ")
        if _c == 1:
            if St == Quest_Status.Active or St == Quest_Status.Passive:
                status[a_].status = Quest_Status.Active if St == Quest_Status.Passive else Quest_Status.Passive
            if St == Quest_Status.Done:
                status[a_].complete(Char)
                status[a_].logs.append('Added 20 xp')
                status[a_].logs.append('Completed on Day - %d' % Char.time.day)
                status[a_].logs.append('Dropped shit')
            if St == Quest_Status.Completed:
                for l in status[a_].logs:
                    print(l)
                input()
        else:
            return


def Quests_info(Char):
    while True:
        head = """
╔════════════╦═══════════════╦══════════╦═══════════════╗
║ ○ ACTIVE   ║  ○ PASSIVE    ║ ○ DONE   ║ ○ COMPLETED   ║
╠════════════╩═══════════════╩══════════╩═══════════════╣
"""

        funcs.clear()
        In.write(head)
        print("║      v             v            v             v       ║")
        print("║     |1|           |2|          |3|           |4|      ║")
        print("╚════════════════════════════════════════════════ Quit ═╝")
        q_ = funcs.Input(">> ")
        if q_ == "quit" or q_ > 4 or q_ < 1:
            return

        funcs.clear()
        head = head.replace("○ %s" % Quest_Status(q_).name.upper(), "◉ %s" % Quest_Status(q_).name.upper())
        In.write(head)
        print_status(Char, head, Quest_Status(q_))


def update(data, Char):  # Char.Quest
    for qst in Char.Quests:
        Done = True
        if qst.status == Quest_Status.Active:
            qst.check_gold(Char)
            qst.check_xp_lvl(Char)
            qst.check_Item(Char)
            qst.check_kills(Char)
            qst.check_town(data.Loc)
            qst.check_time(Char)
        for compl in qst.triger:
            if not compl[1]:  # if task is false
                Done = False
        if Done and qst.status != Quest_Status.Completed:
            qst.status = Quest_Status.Done


def add_quest(Char, data):
    q_names = [x.name for x in Char.Quests]

    for dtq in data.Quests:
        if dtq.name not in q_names:
            dtq.status = Quest_Status.Active
            Char.Quests.append(dtq)
            return dtq.info
    return False


def Wizard(Char, data):
    wizard_msgs = ["Hello my friend... Good to see you!!!", " you fuckin bitch!!!", " I'm gonna cast u in da pussy"]
    funcs.clear()
    for chq in Char.Quests:
        if (chq.status != Quest_Status.Done and chq.status != Quest_Status.Completed):
            funcs.print_cloud("Come back later...", "Wizardo")
            return
    info = add_quest(Char, data)
    if not info:
        funcs.print_cloud("ASDASD", 'Wizardo')
        return

    funcs.print_cloud(choice(wizard_msgs), 'Wizardo')
    funcs.print_cloud(info, 'Wizardo')
