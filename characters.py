#!/usr/bin/python
import base
import random
from items import Inventory
from datetime import datetime


class Character(Inventory):

    #Constructor
    def __init__(self, name=None, strn=None, att=None, hlth=None, sta=None, spd=None, defn=None,
                 xp=None, mxp=None, lvl=None, gold=None, isplr=None, kills=None, time=datetime(1629, 1, 1, 9)):
        self.name = name
        self.strength = strn
        self.attack = att
        self.health = hlth
        self.stamina = sta
        self.speed = spd
        self.defence = defn
        self.xp = xp
        self.max_xp = mxp
        self.lvl = lvl
        self.gold = gold
        self.isplayer = isplr
        self.kills = kills
        self.info = [None] * 10
        self.Quests = []
        self.time = time  # 1629-09-12 09:00:00
        super(Character, self).__init__()

    def sub_speed(self):
        self.speed = self.speed - random.randint(0, 20)  # - speed

    def inc_speed(self):
        self.speed = self.speed + random.randint(0, 20)  # + speed

    # Slap det bitch
    def hit(self):
        hh = 0
        if self.attack < self.strength:
            hh = random.randint(self.attack, self.strength + 1)
        else:
            hh = random.randint(self.strength, self.attack + 1)
        print("Damaged %d hp" % hh)

        return hh

# Level Up
    def level_up(self):
        while self.xp > self.max_xp:
            self.lvl = self.lvl + 1
            #self.xp = self.xp - self.max_xp

            if self.lvl < 2:
                self.max_xp += random.randint(self.max_xp,
                                              self.max_xp + self.max_xp / 2)
            else:
                self.max_xp += random.randint(self.max_xp,
                                              self.max_xp + round(self.max_xp * 0.6))
                                            #i have no idea what i'm doing
            #self.health = Functions.Function.MAX_HEALTH
            # Upgrade health and speed
            self.strength += random.randint(int(self.strength * (1 / self.lvl)), self.strength)
            self.health += random.randint(int(self.health * (2 / self.lvl)), self.health)
            self.speed += random.randint(int(self.speed * (1 / self.lvl)), self.speed)
            base.invSize += 5

    # Add XP
    def addxp(self, xx):

        self.xp = self.xp + xx
        if self.xp >= self.max_xp:
            self.level_up()
            return True

    def GoldToXp(self, gold):
        xp = round(gold / 3)  # currency :D
        self.gold -= gold
        input("You Earned %d Xp" % xp)
        self.addxp(xp)

    # Info
    def update_info(self):
        self.info[0] = "++++++++++++++++++++++++"
        self.info[1] = "| Name:      %s" % self.name.ljust(11)
        self.info[2] = "| Strength:  %s+ %s" % (str(self.strength).ljust(4), str(self.attack).ljust(5))
        self.info[3] = "| Health:    %s+ %s" % (str(self.health).ljust(4), str(self.defence).ljust(5))
        self.info[4] = "| Speed:     %s+ %s" % (str(self.speed).ljust(4), str(self.stamina).ljust(5))
        self.info[5] = "| Exp:      (%s/%s) " % (str(self.xp).rjust(4), str(self.max_xp).ljust(4))
        self.info[6] = "| Level:     %s" % (str(self.lvl).ljust(11))
        self.info[7] = "| Gold:      %s" % (str(self.gold).ljust(11))
        self.info[8] = "| Kills:     %s" % (str(self.kills).ljust(11))
        self.info[9] = "++++++++++++++++++++++++"

    # Print Info
    def print_info(self):
        for i in self.info:
            if i == self.info[0] or i == self.info[-1]:  # printing table
                print(i + "+")
            else:
                print(i + "|")


class Human(Character):

    def __init__(self):
        self.name = "human"
        self.attack = 0
        self.stamina = 0
        self.defence = 0
        self.strength = 15 + self.attack
        self.speed = 130 + self.stamina
        self.health = 50 + self.defence
        self.xp = 0
        self.max_xp = 100
        self.gold = 100
        self.lvl = 1
        self.isplayer = False
        self.info = [None] * 10
        self.kills = 0
        self.Quests = []
        self.time = datetime(1629, 1, 1, 9)
        self.update_info()
        super(Character, self).__init__()


class Orc(Character):

    def __init__(self):
        self.name = "orc"
        self.attack = 0
        self.stamina = 0
        self.defence = 0
        self.strength = 25 + self.attack
        self.speed = 150 + self.stamina
        self.health = 40 + self.defence
        self.xp = 0
        self.max_xp = 100
        self.gold = 100
        self.lvl = 1
        self.isplayer = False
        self.info = [None] * 10
        self.kills = 0
        self.Quests = []
        self.time = datetime(1629, 1, 1, 9)
        self.update_info()
        super(Character, self).__init__()


class Elf (Character):

    def __init__(self):
        self.name = "elf"
        self.attack = 0
        self.stamina = 0
        self.defence = 0
        self.strength = 20 + self.attack
        self.speed = 130 + self.stamina
        self.health = 100 + self.defence
        self.xp = 0
        self.max_xp = 100
        self.gold = 10000
        self.lvl = 1
        self.isplayer = False
        self.kills = 0
        self.info = [None] * 10
        self.Quests = []
        self.time = datetime(1629, 1, 1, 9)
        self.update_info()
        super(Character, self).__init__()
