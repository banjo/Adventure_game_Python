from random import randint
import funcs
from characters import Character
from sys import stdout as Out
#import quests
#++++++++++++++++++++++ B A T T L E +++++++++++++++++++++++++++++


# if someone's dead ...prints winner
def dead(char1, char2):

    char1.kills += 1
    if char1.addxp(40):  # if leveled up  # give xp
        char2.level_up()
    char2.addxp(10)
    char1.gold = char1.gold + randint(30, 100)  # give gold
    char2.health = 0
    print("Enemy recoveried!\n")

    print()
    print("\t   > WINNER < ")
    print("\t->############<-")
    print("\t->#" + char1.name.center(10) + "#<-")
    print("\t->############<-\n")
    print("Press enter to continue...")

    input()
    funcs.day_count(char1, 0, 1, 0)
    return


#if attacked and how much
def attack(p1, p2):

    if (p1.speed + p1.stamina) >= (p2.speed + p2.stamina):  # if you are faster

        hp = 0
        hp = p1.hit()  # hit power
        d = p2.defence - hp
        h = p2.health - hp

        if p2.defence > 1:
            p2.defence = d
        else:
            p2.health = h

        if p2.defence < 1:
            p2.defence = 0

        if p2.health < 1:  # if enemy is dead
            dead(p1, p2)
            return True

        p2.inc_speed()  # increase speed
        print("%s's health: %d | \n" % (p2.name, p2.health + p2.defence))
        p1.sub_speed()  # decrease speed
        return False

    else:
        return attack(p2, p1)


#same defence
def defence(p1, p2):

    hp = p2.hit() / 2

    if (p1.defence > 1):
        p2.defence -= hp
    else:
        p1.health -= hp

    if(p1.defence < 1):
        p1.defence = 0

    if(p1.health < 1):

        dead(p2, p1)
        return True

    p1.inc_speed()  # increase speed
    p1.inc_speed()  # increase speed
    print("%s's health: %d | \n" % (p1.name, p1.health + p1.defence))
    print("Blocked %d\n" % hp)
    p2.sub_speed()

    return False


#start battle...
def battle(char1, char2):

    reset1 = char2.health
    reset2 = char2.speed
    reset3 = char1.speed

    while(True):

        print(char1.name + " VS " + char2.name)
        print("Make your Command!!! \n")
        print(" 1. ATTACK \n")
        print(" 2. FULL ATTACK\n")
        print(" 3. DEFENCE \n")
        print(" 4. SURRENDER \n")

        comm = funcs.Input(">> ")
        funcs.clear()
        if comm == 1 or comm == 2:
            while(True):
                if(attack(char1, char2)):
                    char2.health = reset1
                    char2.speed = reset2
                    return True
                if(comm == 1):
                    break

        elif comm == 3:  # funqciebshia gasaketebeli..jer mezareba :D
            if(defence(char1, char2)):
                char1.speed = reset3
                return True
        elif comm == 4:
            print("RUN YOU FOOL !!!!\n")
            input()
            return False
        else:
            return


# battle map... random enemy
def morph(plrs):

    N = 15
    M = 25

    v = [['' for i in range(30)] for j in range(40)]

    a = 5
    b = 5

    c = 8
    d = 10

    e = 2
    f = 17

    g = 3
    h = 2

    X = plrs[0]
    Y = plrs[1]
    Z = plrs[2]
    enemy = Character()

    #return enemy  =  X#morph disabled

    while(True):
        for i in range(N):
            for j in range(M):
                if i == 0 or i == N - 1:
                    v[i][j] = "="
                elif(j == 0 or j == M - 1):
                    v[i][0] = "|"
                    v[i][M - 1] = "|"
                else:
                    v[i][j] = " "

        v[a][b] = 'Y'  # you
        v[c][d] = 'H'  # human
        v[e][f] = 'O'  # orc
        v[g][h] = 'E'  # elf

        for i in range(N):
            for j in range(M):
                Out.write(v[i][j])
            Out.write("\n")
        Out.write("\033[F" * N)

        v[a][b] = ' '
        v[c][d] = ' '
        v[e][f] = ' '
        v[g][h] = ' '

        a -= 1 if a > N - 4 else randint(-1, 1)
        a += 1 if a < 2 else randint(-1, 1)

        b -= 1 if b > M - 4 else randint(-1, 1)
        b += 1 if b < 2 else randint(-1, 1)

        c -= 1 if c > N - 4 else randint(-1, 1)
        c += 1 if c < 2 else randint(-1, 1)

        d -= 1 if d > M - 4 else randint(-1, 1)
        d += 1 if d < 2 else randint(-1, 1)

        e -= 1 if e > N - 4 else randint(-1, 1)
        e += 1 if e < 2 else randint(-1, 1)

        f -= 1 if f > M - 4 else randint(-1, 1)
        f += 1 if f < 2 else randint(-1, 1)

        g -= 1 if g > N - 4 else randint(-1, 1)
        g += 1 if g < 2 else randint(-1, 1)

        h -= 1 if h > M - 4 else randint(-1, 1)
        h += 1 if h < 2 else randint(-1, 1)
        if(a == c and b == d):
            enemy = X
            break

        elif(a == e and b == f):
            enemy = Y
            break

        elif(a == g and b == h):
            enemy = Z
            break

    # End of While
    Out.write("\033[E" * N)
    return enemy
# morph


 # ???? MAX_HEALTH = 100
# Arena
def arena(Char, data):
    while True:
        funcs.clear()
        funcs.printChars(data.Chars)  # printcharacters properties

        print("Press any key to continue.. q to EXIT ARENA")
        exit = input()
        if (exit == "q"):
            return
        elif Char.health < 1:
            print("You have to recover until you battle again!")
            input()
            return
        elif not battle(Char, morph(data.Chars)):
            return
        funcs.dropper(Char, data.Itms)

    """
    for (i  =  0 i < MAX_HEALTH i++)
        print("\r 0 health  =  1", Char.name, Char.health)
        Char.health  =  i
        System.Threading.Thread.Sleep (1000)

"""
    return
