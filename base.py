import funcs
import items
import battle
import shop
import quests
from items import Item
from items import Locations
from characters import Human, Orc, Elf
from quests import Quest
from sys import stdout as Out
import datetime


class Base(object):
    Chars = [Human(), Orc(), Elf()]
    Itms = []
    Loc = []
    Msgs = []
    Quests = []

"""++++++++++++++++++ G  L O B A L    F U N C T I O N S+++++++++++++++"""

cur_Loc = 0  # CURRENT LOCATION
invSize = 10  # Characters inventory size


#  Input Objects From File
def inputObjects():
    stream = open("Data/objects.dat", "r")  # readonly
    lines = stream.readlines()
    for line in lines:
        lineSplit = line.split(',')  # split lines [ , , , ]
        stuff = Item(lineSplit[0], lineSplit[1],
                     int(lineSplit[2]), int(lineSplit[3]),
                     int(lineSplit[4]), int(lineSplit[5]), int(lineSplit[6]))

        Base.Itms.append(stuff)


#  Input Locations From File
def inputLoc():
    f = open("Data/Locations.dat", "r")
    lines = f.readlines()
    for line in lines:
        lineSplit = line.split(',')
        _loc = Locations(lineSplit[0], lineSplit[1],
                         int(lineSplit[2]), True)
        Base.Loc.append(_loc)

    f.close()


#  Input Location's Functions From File
def inputLocFuncs():
    # Module: build in Functions
    functions = {
        funcs: dir(funcs),
        items: dir(items),
        battle: dir(battle),
        shop: dir(shop),
        quests: dir(quests)
        #base: dir(base)
    }

    ff = open("Data/LocationFuncs.dat", "r")
    lines = ff.readlines()
    i = 0
    for line in lines:
        fns = line.split(',')
        for fun in fns:
            for names in functions:  # iterate in methods
                if fun in functions[names]:
                    Base.Loc[i].func.append(getattr(names, fun))
                # append function
        i = i + 1

    ff.close()


def inputMessages():
    welcomeMsgs = []
    with open("Data/wel-msgs.dat") as wlcMsgs:
        for line in wlcMsgs:
            welcomeMsgs.append(line)

    wlcMsgs.close()
    Base.Msgs = welcomeMsgs


def inputQuests(dat, qq=open("Data/quests_Data.dat", "r")):

    while len(qq.readline()) > 0:
            name = qq.readline().rstrip('\n')
            info = qq.readline().rstrip('\n')
            task_count = int(qq.readline().rstrip('\n'))
            tasks = []
            for count in range(task_count):
                task = qq.readline().rstrip('\n')
                trg = task.split(",")  # task one,1|task two,1
                trg[1] = bool(int(trg[1]))
                tasks.append(trg)
            status = qq.readline().rstrip('\n')
            diff = qq.readline().rstrip('\n')
            quest = Quest(name, info, tasks, status, diff)
            if dat.__class__.__name__ == "Base":
                dat.Quests.append(quest)
            else:  # char's
                dat.Quests.append(quest)
    qq.close()


# =-=-=-=-=--=-=-=-=-= M E N U -=-=-=-=-=-=-=-=-=-=
def MENU(data):

    #MENUFUNCS = {x: data.Loc[cur_Loc].func[x] for x in range(5)}

    # Load(*Data)
    Char = object  # you

    # checking Race
    for player in data.Chars:
        if player.isplayer:
            Char = player

    if Char.name in ['human', 'orc', 'elf']:
        print("Something Wrong!")
        return

    data.Loc[0].locked = False  # Unlock Nemertea
    while True:
        MENUFUNCS = {
            1: data.Loc[cur_Loc].func[0],
            2: data.Loc[cur_Loc].func[1],
            3: data.Loc[cur_Loc].func[2],
            4: data.Loc[cur_Loc].func[3]
        }
        #MENUFUNCS = {x: data.Loc[cur_Loc].func[x] for x in range(4)}

        now = datetime.datetime.now()
        funcs.messeges(Char, data)
        print("Day - %d" % (Char.time.day + ((Char.time.month - 1) * 30)))
        print("\nLocation: " + data.Loc[cur_Loc].loc_name.ljust(22) + "")
        print("              <<<</  \\>>>>")
        print("[<<<<<<<<<<<<<<<<<{||}>>>>>>>>>>>>>>>>>]")
        print("|******-------------------------*******|")
        print("|****-----Choose what U want------*****|")
        print("|***------1.%s****| " % MENUFUNCS[1].__name__.upper().ljust(23, "-"))
        print("|**-------2.%s***|  " % MENUFUNCS[2].__name__.upper().ljust(24, "-"))
        print("|**-------3.%s***|  " % MENUFUNCS[3].__name__.upper().ljust(24, "-"))
        print("|***------4.%s****| " % MENUFUNCS[4].__name__.upper().ljust(23, "-"))
        print("|****-----5.%s*****|" % "Save".ljust(22, "-"))
        print("|******-------------------------*******|")
        print("[<<<<<<<<<<<<<<<<<{||}>>>>>>>>>>>>>>>>>]")
        print("              <<<<\\  />>>> ")

        x = funcs.Input(">> ")
        if x == 5:
            funcs.Save(Char, data)
            s = ""
            Out.write("Wanna exit? (Absolutely Yes, I'm Sure! / n): ")
            while s != "Absolutely Yes, I'm Sure!":
                s = input()
                if s == "n":
                    break
            else:
                exit(0)
        elif x > 5 or x < 1:
            input("Obama is watching you...")
            continue
        else:
            MENUFUNCS[x](Char, data)
        mints = datetime.datetime.now() - now
        if Char.time.hour >= 1 and Char.time.hour < 6:
            funcs.day_count(Char, 0, 8, 0)

        funcs.day_count(Char, 0, 0, (10 * mints.seconds))
